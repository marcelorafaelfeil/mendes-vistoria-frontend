import {NgModule} from '@angular/core';
import {CnpjPipe} from "./pipes/cnpj.pipe";
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {BreadcrumbService} from "./breadcrumb/breadcrumb.service";
import { LocationService } from './services/location.service';

@NgModule({
	imports: [
		CommonModule,
		RouterModule
	],
	providers: [
		CnpjPipe,
		BreadcrumbService,
		LocationService
	],
	declarations: [BreadcrumbComponent],
	exports: [BreadcrumbComponent]
})
export class SharedModule {}
