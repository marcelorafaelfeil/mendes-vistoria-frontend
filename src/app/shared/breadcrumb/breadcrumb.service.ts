import { Injectable, OnInit } from '@angular/core';
import { IBreadcrumb } from "./ibreadcrumb";
import { ActivatedRouteSnapshot, Router } from "@angular/router";

@Injectable({
	providedIn: 'root'
})
export class BreadcrumbService implements OnInit {

	public breadcrumbs: IBreadcrumb[];

	constructor(private router: Router) { }

	ngOnInit() { }

	public generateBreadcrumb(snapshot: ActivatedRouteSnapshot): IBreadcrumb[] {
		let breadcrumbs: IBreadcrumb[] = <IBreadcrumb[]>[];

		if (snapshot) {
			if (snapshot.firstChild) {
				breadcrumbs = breadcrumbs.concat(this.generateBreadcrumb(snapshot.firstChild));
			}
			if (snapshot.url.length) {
				let url = snapshot.url[0].path;

				if(snapshot.data['breadcrumb'] !== undefined) {
					url = snapshot.data['breadcrumb'] + url;
				}
				console.log(snapshot.data, url);

				breadcrumbs.push({
					label: snapshot.data['label'],
					icon: '',
					url: url,
					urlSegments: snapshot.url,
					breadcrumb: snapshot.data['breadcrumb'],
					params: snapshot.params
				});
			}
		}

		return breadcrumbs;
	}
}
