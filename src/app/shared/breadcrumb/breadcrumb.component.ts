import {Component, OnInit} from '@angular/core';
import {IBreadcrumb} from "./ibreadcrumb";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {BreadcrumbService} from "./breadcrumb.service";
import {filter} from "rxjs/operators";

@Component({
	selector: 'ngx-breadcrumb',
	templateUrl: './breadcrumb.component.html',
	styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

	public breadcrumbs: IBreadcrumb[];

	constructor(
		private router: Router,
		private breadcrumbService: BreadcrumbService,
		private activeRoute: ActivatedRoute
	) {
		if(this.router.events !== undefined) {
			console.log(this.router.events);
			this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(
				(routeChange) => {
					this.breadcrumbs = this.breadcrumbService.generateBreadcrumb(this.activeRoute.snapshot);

					this.breadcrumbs.reverse().map((item, i) => {
						return item;
					});
				}
			);
		}
	}

	ngOnInit() {
	}

}
