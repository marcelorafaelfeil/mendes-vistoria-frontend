export interface IHttpPageResponse<T> {
	content: T;
	pageable: {
		sort: {
			sorted: boolean,
			unsorted: boolean,
			empty: boolean
		},
		pageSize: number,
		pageNumber: number,
		offset: number,
		paged: boolean,
		unpaged: boolean
	};
	last: boolean;
	totalPages: number;
	totalElements: number;
	first: boolean;
	sort: {
		sorted: boolean,
		unsorted: boolean,
		empty: boolean
	};
	number: number;
	numberOfElements: number;
	size: number;
	empty: number;
}
