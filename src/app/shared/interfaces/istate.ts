import { ICountry } from "./icountry";

export interface IState {
    id: number;
    name: string;
    uf: string;
    country: ICountry;
}