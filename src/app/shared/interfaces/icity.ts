import { IState } from "./istate";

export interface ICity {
    id: number;
    name: string;
    state: IState;
}