export interface IAddress {
    postalCode: string;
    number: number;
    street: string;
    neighborhood: string;
    complement: string;
    reference: string;
    city: number;
}