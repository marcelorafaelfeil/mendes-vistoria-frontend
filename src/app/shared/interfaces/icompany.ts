import { IAddress } from "./iaddress";

export interface ICompany {
	id: number;
	name: string;
	socialReason: string;
	cnpj: string;
	register: string;
	foundationDate: string;
	email: string;
	address: IAddress;
	enabled: boolean;
	companyNonLocked: boolean;
	disabledDate: Date;
	lockedDate: Date;
}
