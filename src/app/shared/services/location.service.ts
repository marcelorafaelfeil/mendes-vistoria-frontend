import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IState } from '../interfaces/istate';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';
import { IHttpResponse } from '../interfaces/ihttp-response';
import { ICity } from '../interfaces/icity';

@Injectable({
	providedIn: 'root'
})
export class LocationService {

	constructor(
        private _http: HttpClient
    ) {
    }
    
    public getStates$(): Observable<IState[]> {
        return this._http.get(environment.api.public.state).pipe(map((e: IHttpResponse<IState[]>) => { return e.content}));
    }

    public getCities$(state: IState): Observable<ICity[]> {
        let url = environment.api.public.city;
        url = url.replace('{state}', state.id.toString());
        return this._http.get(url).pipe(map((c: IHttpResponse<ICity[]>) => {return c.content}));
    }

}
