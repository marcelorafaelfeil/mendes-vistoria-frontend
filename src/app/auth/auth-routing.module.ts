import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {LoginComponent} from "./login/login.component";
import {LogoutComponent} from "./logout/logout.component";
import {AuthComponent} from "./auth/auth.component";
import {RequestPasswordComponent} from "./request-password/request-password.component";
import {ResetPasswordComponent} from "./reset-password/reset-password.component";

export const routes: Routes = [
	{
		path: '',
		component: AuthComponent,
		children: [{
			path: 'login',
			component: LoginComponent
		},{
			path: 'logout',
			component: LogoutComponent
		},{
			path: 'request-password',
			component: RequestPasswordComponent
		},{
			path: 'reset-password',
			component: ResetPasswordComponent
		}]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class AuthRoutingModule {
}
