import {Component, OnInit} from '@angular/core';
import {NbAuthComponent} from "@nebular/auth";

@Component({
	selector: 'ngx-auth',
	template: `
		<nb-layout>
			<nb-layout-column>
				<nb-auth-block>
					<nb-card>
						<nb-card-body>
							<router-outlet></router-outlet>
						</nb-card-body>
					</nb-card>
				</nb-auth-block>
			</nb-layout-column>
		</nb-layout>
	`,
	styleUrls: ['./auth.component.scss']
})
export class AuthComponent extends NbAuthComponent {

}
