import { Component, OnInit } from '@angular/core';
import {NbLogoutComponent} from "@nebular/auth";

@Component({
  selector: 'ngx-logout',
  template: `
	  <h1 id="title" class="title">LogOut</h1>
	  <p class="sub-title">Aguarde enquanto cuidamos disso...</p>
  `
})
export class LogoutComponent extends NbLogoutComponent {
}
