import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {NbAuthService, NbAuthToken} from "@nebular/auth";
import {Observable} from "rxjs";
import {switchMap} from "rxjs/operators";

@Injectable({
	providedIn: 'root'
})
export class JwtInterceptorService implements HttpInterceptor {

	constructor(
		private authService: NbAuthService
	) {
	}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		return this.authService.isAuthenticatedOrRefresh()
			.pipe(
				switchMap(authenticated => {
					if (authenticated) {
						return this.authService.getToken().pipe(
							switchMap((token: NbAuthToken) => {
								const JWT = `Bearer ${token.getValue()}`;
								req = req.clone({
									setHeaders: {
										Authorization: JWT,
									},
								});
								return next.handle(req);
							}),
						)
					} else {
						// Request is sent to server without authentication so that the client code
						// receives the 401/403 error and can act as desired ('session expired', redirect to login, aso)
						return next.handle(req);
					}
				}),
			)
	}
}
