import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {
	NbAlertModule,
	NbButtonModule,
	NbCardModule,
	NbCheckboxModule,
	NbInputModule,
	NbLayoutModule
} from "@nebular/theme";
import {AuthRoutingModule} from "./auth-routing.module";
import {NbAuthModule} from "@nebular/auth";
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { AuthComponent } from './auth/auth.component';
import { RequestPasswordComponent } from './request-password/request-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

@NgModule({
	declarations: [LoginComponent, LogoutComponent, AuthComponent, RequestPasswordComponent, ResetPasswordComponent],
	imports: [
		CommonModule,
		NbLayoutModule,
		NbCardModule,
		FormsModule,
		RouterModule,
		NbAlertModule,
		NbInputModule,
		NbButtonModule,
		NbCheckboxModule,
		AuthRoutingModule,
		NbAuthModule
	]
})
export class AuthModule {
}
