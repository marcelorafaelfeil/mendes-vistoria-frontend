import {NgModule} from '@angular/core';

import {PagesComponent} from './pages.component';
import {PagesRoutingModule} from './pages-routing.module';
import {ThemeModule} from '../@theme/theme.module';
import {MiscellaneousModule} from './miscellaneous/miscellaneous.module';
import {HomeComponent} from './home/home.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {SharedModule} from "../shared/shared.module";

const PAGES_COMPONENTS = [
	PagesComponent,
	HomeComponent
];

@NgModule({
	imports: [
		PagesRoutingModule,
		ThemeModule,
		SharedModule,
		SharedModule,
		MiscellaneousModule
	],
	declarations: [
		...PAGES_COMPONENTS,
		DashboardComponent,
	]
})
export class PagesModule {
}
