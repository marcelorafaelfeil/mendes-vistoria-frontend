import {NgModule} from '@angular/core';
import {EntitiesComponent} from './entities.component';
import {EntitiesRoutingModule} from "./entities-routing.module";
import {SharedModule} from "../../shared/shared.module";

@NgModule({
	declarations: [EntitiesComponent],
	imports: [
		EntitiesRoutingModule,
		SharedModule
	]
})
export class EntitiesModule {
}

