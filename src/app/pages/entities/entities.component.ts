import {Component, OnInit} from '@angular/core';

@Component({
	selector: 'ngx-entities',
	template: `
		<ngx-breadcrumb></ngx-breadcrumb>
		<router-outlet></router-outlet>
	`,
	styles: [],
})
export class EntitiesComponent implements OnInit {

	constructor() {
	}

	ngOnInit() {
	}

}
