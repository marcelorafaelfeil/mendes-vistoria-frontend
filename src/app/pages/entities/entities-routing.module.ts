import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EntitiesComponent} from './entities.component';


const routes: Routes = [{
	path: '',
	component: EntitiesComponent,
	children: [{
		path: 'company',
		data: { label: 'Empresas', breadcrumb: 'pages/entities/' },
		loadChildren: 'app/pages/entities/company/company.module#CompanyModule'
	}]
}];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class EntitiesRoutingModule {
}
