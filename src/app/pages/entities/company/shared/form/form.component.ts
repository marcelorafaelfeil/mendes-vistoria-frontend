import {Component, OnInit, Input} from '@angular/core';
import { ICompany } from '../../../../../shared/interfaces/icompany';
import { IAddress } from '../../../../../shared/interfaces/iaddress';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { CompanyService } from '../../company.service';
import { NbToastrService } from '@nebular/theme';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { Router } from '@angular/router';
import { LocationService } from '../../../../../shared/services/location.service';
import { IState } from '../../../../../shared/interfaces/istate';
import { ICity } from '../../../../../shared/interfaces/icity';
import { Subject } from 'rxjs';

@Component({
	selector: 'ngx-form',
	templateUrl: './form.component.html',
	styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

	@Input("title")
	public title: string = 'Formulário de empresas';
	@Input("description")
	public description: string = "";
	public form: FormGroup;
	public states: IState[];
	public cities: ICity[];
	public isLoading: boolean;
	private company: ICompany;
	private submitted: boolean;
	private _logo: File;
	private _background: File;
	private _finished: Subject<boolean>;
	private _finishedData: Subject<ICompany>;
	private _finishedLogo: boolean;
	private _finishedBackground: boolean;
	private _finishedLogo$: Subject<boolean>;
	private _finishedBackground$: Subject<ICompany>;

	constructor(
		private _router: Router,
		private _fb: FormBuilder,
		private _comSer: CompanyService,
		private _toastr: NbToastrService,
		private _locSer: LocationService
	) {
		this.submitted = false;
		this.isLoading = false;
		this.states = [];
		this.cities = [];
		this.company = <ICompany>{
			address: <IAddress>{}
		};
		this._locSer.getStates$().subscribe((e) => {
			this.states = e;
		});
		this._finished = new Subject();
		this._finishedData = new Subject();
		this._finishedLogo$ = new Subject();
		this._finishedBackground$ = new Subject();
	}

	ngOnInit() {
		this.form = this._fb.group({
			name: ['', Validators.required],
			socialReason: [''],
			cnpj: ['', Validators.required],
			register: [''],
			foundationDate: [''],
			email: [''],
			address: this._fb.group({
				postalCode: ['', Validators.required],
				number: [''],
				street: [''],
				neighborhood: [''],
				complement: [''],
				reference: [''],
				city: ['', Validators.required],
				uf: ['', Validators.required]
			})
		});

		this._finishedBackground = false;
		this._finishedLogo = false;
		
		this._finished.subscribe(() => {
			if(this._finishedLogo && this._finishedBackground) {
				this._toastr.show('Empresa cadastrada com sucesso!', 'Concluído', {status: NbToastStatus.SUCCESS})
				this._router.navigate(['pages/entities/company']);
				this.isLoading = false;
			}
		});

		this._finishedBackground$.subscribe((data) => {
			if(this._logo !== undefined) {
				this._comSer.uploadImage(data.id, this._logo, 0).subscribe(() => {
					this._finishedLogo = true;
					this._finished.next();
				});
			} else {
				this._finishedLogo = true;
				this._finished.next();
			}
		})

		this._finishedData.subscribe((data) => {
			if(this._background !== undefined) {
				this._comSer.uploadImage(data.id, this._background, 1).subscribe(() => {
					this._finishedBackground = true;
					this._finishedBackground$.next(data);
				});
			} else {
				this._finishedBackground = true;
				this._finishedBackground$.next(data);
			}
		});
	}

	get f() {
		return this.form.controls;
	}

	get fa() {
		return this.form.controls.address['controls'];
	}

	onSubmit() {
		if(this.form.valid) {
			this.isLoading = true;
			this._comSer.createCompany$(this.form.getRawValue()).subscribe((data: ICompany) => {
				this._finishedData.next(data);
			}, (error) => {
				this.isLoading = false;
				console.error(error);
			});
		}
	}

	public onStateSelect($event) {
		this._locSer.getCities$($event).subscribe((e) => {
			this.cities = e;
		});
	}

	public onLogoFileChange($event) {
		let reader = new FileReader();

		if($event.target.files && $event.target.files.length) {
			const file = $event.target.files[0];
			reader.readAsDataURL(file);
			this._logo = file;
		}
	}

	public onBackgroundFileChange($event) {
		let reader = new FileReader();

		if($event.target.files && $event.target.files.length) {
			const file = $event.target.files[0];
			reader.readAsDataURL(file);
			this._background = file;
		}
	}

}
