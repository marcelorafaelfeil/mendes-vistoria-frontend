import {Inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IHttpPageResponse} from "../../../shared/interfaces/ihttp-page-response";
import {ICompany} from "../../../shared/interfaces/icompany";
import {environment} from "../../../../environments/environment";
import {tap, map} from "rxjs/operators";
import {IPage} from "../../../shared/interfaces/ipage";
import { FormGroup } from '@angular/forms';
import { IHttpResponse } from '../../../shared/interfaces/ihttp-response';

@Injectable({
	providedIn: 'root'
})
export class CompanyService {

	constructor(
		@Inject(HttpClient) private _http: HttpClient
	) {
	}

	public getCompanies$(page: IPage): Observable<IHttpPageResponse<ICompany[]>> {
		return this._http.get(environment.pages.entities.company.list, {
			params: {
				page: page.page.toString(),
				size: page.size.toString()
			}
		}).pipe(
			tap((data: IHttpPageResponse<ICompany[]>) => {
				return data;
			})
		)
	}

	public createCompany$(form: ICompany): Observable<ICompany> {
		return this._http.post(environment.pages.entities.company.create, form).pipe(
			map((data: IHttpResponse<ICompany>) => {
				return data.content;
			})
		)
	}

	public uploadImage(id: number, file: File, type: number): Observable<boolean> {
		let url = type === 0 ? environment.pages.entities.company.uploadLogo : environment.pages.entities.company.uploadBackground;
		url = url.replace('{id}', id.toString());
		const fd: FormData = new FormData();
		fd.append("file", file);
		return this._http.post(url, fd, {
			reportProgress: true
		}).pipe(map(() => {
			return true;
		}))
	}

	public parse() {

	}
}
