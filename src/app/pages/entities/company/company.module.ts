import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CompanyRoutingModule} from "./company-routing.module";
import {NbCardModule, NbInputModule, NbDatepickerModule, NbSelectModule, NbButtonModule, NbToastrModule} from "@nebular/theme";
import { ListComponent } from './list/list.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {CompanyService} from "./company.service";
import {Ng2SmartTableModule} from "ng2-smart-table";
import {JwtInterceptorService} from "../../../auth/jwt-interceptor.service";
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { FormComponent } from './shared/form/form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
	declarations: [ListComponent, CreateComponent, EditComponent, FormComponent],
	imports: [
		HttpClientModule,
		CommonModule,
		FormsModule,
		NbCardModule,
		NbInputModule,
		NbSelectModule,
		Ng2SmartTableModule,
		NbDatepickerModule,
		NbButtonModule,
		ReactiveFormsModule,
		NbToastrModule,
		CompanyRoutingModule
	],
	providers: [
		CompanyService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: JwtInterceptorService,
			multi: true
		}
	]
})
export class CompanyModule {
}
