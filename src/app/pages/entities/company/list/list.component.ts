import {Component, OnInit} from '@angular/core';
import {CompanyService} from "../company.service";
import {IHttpPageResponse} from "../../../../shared/interfaces/ihttp-page-response";
import {ICompany} from "../../../../shared/interfaces/icompany";
import {Router} from "@angular/router";
import {CnpjPipe} from "../../../../shared/pipes/cnpj.pipe";

@Component({
	selector: 'ngx-list',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

	public settings: Object;
	public source: ICompany[];

	constructor(
		private _comSer: CompanyService,
		private _router: Router,
		private _cnpjPipe: CnpjPipe
	) {
		this.settings = {
			mode: 'external',
			add: {
				addButtonContent: '<i class="nb-plus"></i>',
				createButtonContent: '<i class="nb-checkmark"></i>',
				cancelButtonContent: '<i class="nb-close"></i>',
			},
			edit: {
				editButtonContent: '<i class="nb-edit"></i>',
				saveButtonContent: '<i class="nb-checkmark"></i>',
				cancelButtonContent: '<i class="nb-close"></i>',
			},
			delete: {
				deleteButtonContent: '<i class="nb-trash"></i>',
				confirmDelete: true,
			},
			columns: {
				id: {
					title: 'ID',
					type: 'number',
					width: '10%'
				},
				name: {
					title: 'Nome',
					type: 'string',
				},
				cnpj: {
					title: 'CNPJ',
					type: 'string',
				}
			},
		};
	}

	ngOnInit() {
		this._comSer.getCompanies$({page: 0, size: 100}).subscribe((data: IHttpPageResponse<ICompany[]>) => {
			data.content.forEach(c => {
				c.cnpj = this._cnpjPipe.transform(c.cnpj);
			});
			this.source = data.content;
		});
	}

	public openCreate($event) {
		this._router.navigate(['pages/entities/company/create'])
	}

	public openEdit($event) {
		this._router.navigate(['pages/entities/company/' + $event.data.id + '/edit'])
	}

	public onDeleteConfirm($event) {}

}
