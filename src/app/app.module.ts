/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import {APP_BASE_HREF} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {CoreModule} from './@core/core.module';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {ThemeModule} from './@theme/theme.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AuthGuard} from './auth.guard';
import {JwtInterceptorService} from './auth/jwt-interceptor.service';
import { CnpjPipe } from './shared/pipes/cnpj.pipe';
import { SharedModule } from './shared/shared.module';

@NgModule({
	declarations: [AppComponent, CnpjPipe],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		HttpClientModule,
		AppRoutingModule,
		NgbModule,
		SharedModule,
		ThemeModule.forRoot(),
		CoreModule.forRoot(),
	],
	bootstrap: [AppComponent],
	providers: [
		{provide: APP_BASE_HREF, useValue: '/'},
		AuthGuard,
	],
})
export class AppModule {
}
