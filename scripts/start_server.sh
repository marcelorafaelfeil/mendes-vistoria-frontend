#!/bin/bash

if [ "$DEPLOYMENT_GROUP_NAME" == "release" ]
then
    dir=/home/ubuntu/apps/release/mendes-web/
elif [ "$DEPLOYMENT_GROUP_NAME" == "production" ]
then
    dir=/home/ubuntu/apps/release/mendes-web/
fi

filename=$(ls -dt ${dir}api* | head -1)
nohup node ${dir}index.js > /dev/null 2>&1 &