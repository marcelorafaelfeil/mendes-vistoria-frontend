#!/bin/bash

if [ "$DEPLOYMENT_GROUP_NAME" == "release" ] > /dev/null
then
    search_terms=release/mendes-web
elif [ "$DEPLOYMENT_GROUP_NAME" == "production" ] > /dev/null
then
    search_terms=production/mendes-web
fi

if ps aux | grep "$search_terms" | grep -v 'grep' | awk '{print $2}'
then
    kill $(ps aux | grep "$search_terms" | grep -v 'grep' | awk '{print $2}')
fi