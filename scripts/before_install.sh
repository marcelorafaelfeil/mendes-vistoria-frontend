#!/bin/bash

base=/opt/codedeploy-agent/deployment-root/${DEPLOYMENT_GROUP_ID}/${DEPLOYMENT_ID}/
from=${base}deployment-archive/dist/

if [ "$DEPLOYMENT_GROUP_NAME" == "release" ]
then
    dir=/home/ubuntu/apps/release/mendes-web/
elif [ "$DEPLOYMENT_GROUP_NAME" == "production" ]
then
    dir=/home/ubuntu/apps/release/mendes-web/
fi

rm -R {dir}*

mv -R ${from} ${dir}